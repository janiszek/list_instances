## Install Deps
```
npm install 
```

## Configure
Edit config.json to change Access and Secret Key ID  

## Run
```
node list_instances.js
```

## Output
```
ubuntu@ip-172-31-17-206:~/$ node list_instances.js 
Total Reservations: 1
Name: DeleteMe		Pub. IP: 18.217.169.125
Name: DeleteMe		Pub. IP: 18.217.167.159
Name: DeleteMe		Pub. IP: 13.58.83.132
Name: DeleteMe		Pub. IP: 18.191.219.40
Name: DeleteMe		Pub. IP: 13.59.173.85
```
