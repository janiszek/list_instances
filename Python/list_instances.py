# List the EC2 istanance with Public IP Address within the selected AWS account
#
# Create a user in IAM with Programmatic Access and get the Access Key ID Secret Access Key and Region - replace the parameters in config.py

import boto3

#this line will use the access key id and secrect access key from .aws/credentials [default]
#ec2client = boto3.client('ec2')

# this line will copy the params from config.py file
#import config
#ec2client = boto3.client('ec2', aws_access_key_id = config.AWS_ACCESS_KEY_ID, aws_secret_access_key = config.AWS_SECRET_ACCESS_KEY, 
#    aws_session_token=config.SESSION_TOKEN, region_name = config.AWS_REGION_NAME)

#this line will use the access key id and secrect access key and session token from .aws/credentials [INSERT_PROFILE]
session = boto3.Session(profile_name='INSERT_PROFILE')
ec2client = session.client('ec2')

response = ec2client.describe_instances()

print("InstanceID\t\tPublicIP");
print("---------------------------------------")

for reservation in response["Reservations"]:
    for instance in reservation["Instances"]:
        # The next line will print the entire dictionary object
        #print(instance)
        #  The next line will print only the InstanceId
        print(instance["InstanceId"])
        # The next line will print only the InstanceId and Public IP Address
        #if ("PublicIpAddress" in instance): 
        #    print(instance["InstanceId"] + "\t" + instance["PublicIpAddress"])

